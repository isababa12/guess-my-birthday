from random import randint

name_input = []

name = input("Hi! What is your name? ")
name_input.append(name)

print(f"Hi", name, "may I guess your birthday?")

response = input('Enter "yes", or, "no": ').lower()
if response == "yes":
        print("Awesome! This will be fun...")
else:
        print("Oh okay then...")
        exit()

for guess_number in range(1, 6):
    random_month = randint(1, 12)
    random_year = randint(1924, 2004)

    print(f"Guess {guess_number}, {name}, were you born in {random_month} / {random_year} ?")

    respond_to_guess = input('Yes or No? "yes", "no" : ').lower()

    if respond_to_guess == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have BETTER things to do. GOOD-BYE! >:(")
    else:
        print("Drat! Lemme try again!")
